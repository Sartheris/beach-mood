package com.sarth.beachmood.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class MyPrefs {

	private static SharedPreferences settings;
	private static SharedPreferences.Editor editor;

	private static int image;
	private static boolean isAutoChangeChecked;
	private static boolean isRandomImageChecked;
	private static int timeToNextImage;

	/** Initialization **/
	public static void loadSharedPreferences(Context context) {
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		image = getPreferenceInt("image", 0);
		isAutoChangeChecked = getPreferenceBoolean("isAutoChangeChecked", false);
		isRandomImageChecked = getPreferenceBoolean("isRandomImageChecked", false);
		timeToNextImage = getPreferenceInt("timeToNextImage", 60000);
	}

	/** Getters and Setters **/
	public static int getImage() {
		return image;
	}

	public static void setImage(int image) {
		MyPrefs.image = image;
		setPreferenceInt("image", image);
	}

	public static boolean isAutoChangeChecked() {
		return isAutoChangeChecked;
	}

	public static void setAutoChangeChecked(boolean isAutoChangeChecked) {
		MyPrefs.isAutoChangeChecked = isAutoChangeChecked;
		setPreferenceBoolean("isAutoChangeChecked", isAutoChangeChecked);
	}
	
	public static boolean isRandomImageChecked() {
		return isRandomImageChecked;
	}

	public static void setRandomImageChecked(boolean isRandomImageChecked) {
		MyPrefs.isRandomImageChecked = isRandomImageChecked;
		setPreferenceBoolean("isRandomImageChecked", isRandomImageChecked);
	}
	
	public static int getTimeToNextImage() {
		return timeToNextImage;
	}

	public static void setTimeToNextImage(int timeToNextImage) {
		MyPrefs.timeToNextImage = timeToNextImage;
		setPreferenceInt("timeToNextImage", timeToNextImage);
	}

	/** Preference int **/
	public static int getPreferenceInt(String preferenceName, int defaultValue) {
		int i = settings.getInt(preferenceName, defaultValue);
		return i;
	}

	private static void setPreferenceInt(String preferenceName, int value) {
		editor = settings.edit();
		editor.putInt(preferenceName, value);
		editor.commit();
	}

	/** Preference boolean **/
	public static boolean getPreferenceBoolean(String preferenceName, boolean defaultValue) {
		boolean b = settings.getBoolean(preferenceName, defaultValue);
		return b;
	}

	private static void setPreferenceBoolean(String preferenceName, boolean value) {
		editor = settings.edit();
		editor.putBoolean(preferenceName, value);
		editor.commit();
	}
}