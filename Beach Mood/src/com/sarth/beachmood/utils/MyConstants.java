package com.sarth.beachmood.utils;

import com.sarth.beachmood.R;

public class MyConstants {

	public static final int MAX_IMAGES = 11;
	public static final int MUSIC = R.raw.beach;
	public static final String IMAGE_RESOURCE = "android.resource://" + "com.sarth.beachmood" + "/drawable/" + "beach";
	public static final String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.sarth.beachmood";
	
	public static final String GOOGLE_PLAY_URL_MORE_APPS = "https://play.google.com/store/apps/developer?id=SARTH+INC";
	public static final String DEVELOPER_EMAIL = "sartheris@gmail.com";
}