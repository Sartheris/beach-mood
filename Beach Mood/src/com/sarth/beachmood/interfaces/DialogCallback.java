package com.sarth.beachmood.interfaces;

public interface DialogCallback {

	void onDialogTimeSet();
	void onDialogDismissed();
}