package com.sarth.beachmood.dialogs;

import com.sarth.beachmood.R;
import com.sarth.beachmood.interfaces.DialogCallback;
import com.sarth.beachmood.utils.MyConstants;
import com.sarth.beachmood.utils.MyPrefs;
import com.sarth.beachmood.utils.MyUtils;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MyDialogs {

	private static DialogCallback callback;
	private static AlertDialog dialog;
	private static AlertDialog.Builder builder;
	private static LayoutInflater inflater;
	private static View dialogView;
	
	public static void showAboutDialog(final Context context) {
		builder = new AlertDialog.Builder(context);
		inflater = LayoutInflater.from(context);
		dialogView = inflater.inflate(R.layout.dialog_about, null);
		builder.setTitle(R.string.about_title)
			.setView(dialogView)
			.setPositiveButton(R.string.dialog_about_button_rate, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyConstants.GOOGLE_PLAY_URL));
					context.startActivity(browserIntent);
				}
			})
			.setNeutralButton(R.string.dialog_about_button_more_apps, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyConstants.GOOGLE_PLAY_URL_MORE_APPS));
					context.startActivity(browserIntent);
				}
			})
			.setNegativeButton(android.R.string.cancel, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			;
		dialog = builder.create();
		dialog.setIcon(R.drawable.ic_launcher);
		dialog.show();
	}
	
	public static void showAutoChangeDialog(final Context context, DialogCallback callback) {
		MyDialogs.callback = callback;
		inflater = LayoutInflater.from(context);
		dialogView = inflater.inflate(R.layout.dialog_auto_change, null);
		final EditText et = (EditText) dialogView.findViewById(R.id.etMinutes);
		final CheckBox cb = (CheckBox) dialogView.findViewById(R.id.cbRandomize);
		builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.dialog_auto_change_title)
			.setView(dialogView)
			.setPositiveButton(R.string.dialog_auto_change_button_set, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					int minutesToChangeImage = Integer.parseInt(et.getText().toString());
					if (minutesToChangeImage > 60) {
						minutesToChangeImage = 60;
					} else if (minutesToChangeImage < 1) {
						minutesToChangeImage = 1;
					}
					minutesToChangeImage *= 60000;
					MyPrefs.setTimeToNextImage(minutesToChangeImage);
					MyPrefs.setRandomImageChecked(cb.isChecked());
					MyPrefs.setAutoChangeChecked(true);
					dialog.dismiss();
					Toast.makeText(context, R.string.dialog_auto_change_confirmation, Toast.LENGTH_LONG).show();
					MyDialogs.callback.onDialogTimeSet();
				}
			})
			.setNegativeButton(android.R.string.cancel, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		dialog = builder.create();
		dialog.setIcon(R.drawable.ic_launcher);
		dialog.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				MyDialogs.callback.onDialogDismissed();
			}
		});
		dialog.show();
	}
	
	public static void showReportDialog(final Context context) {
		builder = new AlertDialog.Builder(context);
		inflater = LayoutInflater.from(context);
		dialogView = inflater.inflate(R.layout.dialog_report, null);
		builder.setTitle(R.string.report_problem_title)
			.setView(dialogView)
			.setPositiveButton(R.string.dialog_about_button_report, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					MyUtils.sendReportEmail(context);
				}
			})
			.setNegativeButton(android.R.string.cancel, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			;
		dialog = builder.create();
		dialog.setIcon(R.drawable.ic_launcher);
		dialog.show();
	}
}